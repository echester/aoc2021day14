#
# advent of code 2021 - day 14
#
# lots of comments added because took a while to debug why this wasn't
# working and never want to explain it again. also somewhat broken down
# into stages. this is about 39sloc without comments and empty lines.
#

# preamble
use strict;
use warnings;

# useful modules
use List::Util qw| max min sum |;

# variables
my %counts;		# char counts
my %pairs;		# pair counts
my %newpairs;	# temporary bag for new pairs 
my $templ;		# template input
my @rules;		# list of polymerisation rules

# get input file from command line ...
my $infile = $ARGV[0];
# ... and bork if we can't.
die "\ninput file not specified; borking.\n\n" unless defined $infile;

# get required steps from command line or default to 10
my $steps = defined $ARGV[1] ? $ARGV[1] : 10;

# open the input file
open(my $inf, '<', $infile) || die "cannot open file $infile: $!\n";

# load the input
while(<$inf>) { 
	chomp;
	# get the starting template
	if /^(\w+)$/ { $templ = $1; }
	# populate a list of polymerisation rules 
	# (assuming the rules for part 2 might have multiple insertions)
	elsif /^(\w+)\s+-\>\s+(\w+)$/ { push @rules, [$1, $2]; }
}
close $inf;

# initial run through template counting chars
$counts{$_}++ foreach (split //, $templ);

# run through the template and populate the initial pairs list
for (my $p = 0; $p < length($templ); $p++) {
	# for each position in template, get a 2-char string ...
	my $pair = substr($templ, $p, 2);
	# ... unless we can't any more ...
	last if length($pair)<2;
	# ... and keep it for later.
	$pairs{$pair}++;
}

# the main polymerisation hooplah: run the whole thing a number of times (steps)
for my $step (0 .. $steps-1) {
	# loop over all the pairs in the polymer so far 
	# (this is where it went wrong at first: must not consider new pairs generated until the step is over)
	foreach (keys %pairs) {
		# for this pair, loop over the defined rules
		for my $i (0 .. $#rules) {
			# if there's a rule for this pair...
			if ($rules[$i][0] eq $_) {
				# get instances of this pair in the polymer
				my $c = $pairs{$_};
				# if there are any instances ...
				if $c {
					# use the rule to generate the two new pairs
					my $fnp = substr($_,0,1) . $rules[$i][1];
					my $snp = $rules[$i][1] . substr($_,1,1);
					# store however many of them we need in a temporary bag
					$newpairs{$fnp} += $c;
					$newpairs{$snp} += $c;
					# and remove the same number of the original pair
					$pairs{$_} -= $c;
					# update the counts for the chars added
					$counts{$rules[$i][1]} += $c;
				}
			}
		}
	}
# tip the temporary bag into the polymer pairs ...
%pairs = (%pairs, %newpairs);
# making sure they're all gone:
%newpairs = ();
# ... and go again if needed.
}

# get a list of the char counts
my @o = values(%counts);

# output the polymer length
printf "\ntotal = %d\n", sum(@o);
# and the required difference between max and min values
printf "diff = %d\n\n", max(@o) - min(@o);
